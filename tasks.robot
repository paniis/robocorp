*** Settings ***
Library     RPA.Tables
Library     RPA.Browser.Selenium
Library     Collections
Library     RPA.Windows
Library     RPA.FileSystem
Library     String


*** Tasks ***
Web-Thairath
    Open Available Browser    https://www.thairath.co.th/news/
    Click Element    alias:Span
    ${items_title}=    Create List
    ${items_emp}=    Create List
    ${items_date}=    Create List
    ${items_content}=    Create List
    ${items_img}=    Create List
    ${items_link}=    Create List
    ${items_category}=    Create List
    ${items_tag}=    Create List
    @{path}=    Set Variable    foreign    local    society    politic    crime    auto    tech
    FOR    ${link}    IN    @{path}
        Go To    https://www.thairath.co.th/news/${link}
        FOR    ${i}    IN RANGE    1    13
            Sleep    6s
            Wait Until Element Is Visible    xpath://*[@id="__next"]/main/div/div[3]/div[1]    10
            ${category}=    RPA.Browser.Selenium.Get Text    xpath://*[@id="__next"]/main/div/div[3]/div[1]/div/h1/span
            Append To List    ${items_category}    ${category}
            ${link}=    Get Element Attribute
            ...    xpath://*[@id="__next"]/main/div/aside/div[1]/div/div[${i}]/div/a
            ...    href
            Append To List    ${items_link}    ${link}
            ${img}=    Get Element Attribute
            ...    xpath:/html/body/div[2]/main/div/aside/div[1]/div/div[${i}]/div/a/img
            ...    src
            Append To List    ${items_img}    ${img}
            Wait Until Element Is Enabled    xpath://*[@id="__next"]/main/div/aside/div[1]/h3    10
            Scroll Element Into View    xpath://*[@id="__next"]/main/div/aside/div[1]/div/div[${i}]
            Click Element    xpath://*[@id="__next"]/main/div/aside/div[1]/div/div[${i}]
            Scroll Element Into View    xpath://*[@id="Article_Detail"]/div/div/div[3]/div[1]/div
            Wait Until Element Is Visible    xpath://*[@id="Article_Detail"]/div/div/div[3]/div[1]/div[1]/h1    10
            ${title}=    RPA.Browser.Selenium.Get Text
            ...    xpath://*[@id="Article_Detail"]/div/div/div[3]/div[1]/div[1]/h1
            Append To List    ${items_title}    ${title}
            ${date}=    RPA.Browser.Selenium.Get Text    xpath://*[@id="Article_Detail"]/div/div/div[2]/div[1]
            Append To List    ${items_date}    ${date}
            ${content}=    RPA.Browser.Selenium.Get Text
            ...    xpath://*[@id="Article_Detail"]/div[1]/div/div[3]/div[1]/div[3]/div
            Append To List    ${items_content}    ${content}
            Sleep    3s
            Execute Javascript    window.scrollTo(0, document.body.scrollHeight)
            ${contains_tag}=    Does Page Contain Element
            ...    xpath:/html/body/div[2]/main/div/div[4]/div[1]/div[4]/div/div/div[2]/div/div/a
            IF    ${contains_tag}
                ${count_j}=    Get Element Count
                ...    xpath:/html/body/div[2]/main/div/div[4]/div[1]/div[4]/div/div/div[2]/div/div/a
                Execute Javascript    window.scrollTo(0, document.body.scrollHeight)
                Sleep    2s
                ${str}=    Set Variable
                FOR    ${j}    IN RANGE    1    ${count_j}+1
                    ${tag}=    RPA.Browser.Selenium.Get Text
                    ...    xpath:/html/body/div[2]/main/div/div[4]/div[1]/div[4]/div/div/div[2]/div/div/a[${j}]
                    ${value_tag}=    Set Variable    ${tag}
                    ${str}=    Catenate    ${value_tag}    ,    ${str}
                END
            ELSE
                Run Keyword And Ignore Error    Wait Until Element Is Visible    error
                Run Keyword And Ignore Error    RPA.Browser.Selenium.Get Text    error
                ${str}=    Set Variable    -
            END
            Append To List    ${items_tag}    ${str}
            Go Back
            Sleep    2s
        END
    END
    ${create_table}=    Create Dictionary
    ...    HEADLINE=${items_title}
    ...    TAG=${items_tag}
    ...    DATE=${items_date}
    ...    CONTENT=${items_content}
    ...    IMAGE=${items_img}
    ...    LINK=${items_link}
    ...    CATEGORY=${items_category}
    ${table}=    Create Table    ${create_table}    ${TRUE}
    Write table to CSV    ${table}    Thairath_test.csv    encoding=utf-8-sig
